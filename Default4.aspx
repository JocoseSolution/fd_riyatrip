﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default4.aspx.cs" Inherits="Default4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

  <link href="CSS/foundation.css" rel="stylesheet" />
</head>
<body>
  
 <style type="text/css">
     

/*--------------nav--------------------*/




.top-nav-collapse {
/*    background-color: #606060;*/
	z-index: 4;
}


nav.navbar .top-nav-collapse ul a:hover {
    background-color: transparent;
}

label {
    color: #606060;
    position: relative;
    top: 15px;
}

/*--------------------Filtro------------------------*/
.section-filter{
	margin-top: -30px;
}

.clear-filter{
	  padding: 2px 10px;
    text-transform: none;
    top: 40px;
    font-size: 1.2rem;
    font-weight: 300;
    height: 40px;
}

.clear-filter i.material-icons.left{
    font-size: 1.1rem;
    width: 16px;
    height: 36px;
    line-height: 36px;
    margin-right: 5px;
}


/*------------conteudo manual-------------------*/
 h2.header{
	color:  #606060;
	text-align: center;
}


/*------------Responsivo-------------*/
@media(max-width:992px){
  .scrolling-navbar {
    height: 325px;
  }
	.clear-filter {	
		top:0;			
	}	
}

@media(min-width:600px){
    .scrolling-navbar {
        -webkit-transition: background .5s ease-in-out,padding .5s ease-in-out;
	    transition: background .5s ease-in-out,padding .5s ease-in-out;
	    	padding-top: 20px;
    }
}



/*---- cards style ---*/

/*------------cards-------------------*/
.cardContainer{
	min-width:300px;
	padding-left: 1%;
}

.card .card-image{
	max-width: 45%;
    margin: auto;
    min-height: 250px;
}
    
.card .card-image img{
	min-height: 250px;
}

.card .card-content{
	background-color: white;
}

.card .card-content .card-title {
    font-size: 1.2rem;
}

/*color card*/
.card.cardTerror {
	background-color: #000;
}
.card.cardSuspense {
	background-color: #bdbdbd;
}
.card.cardRomance {
	background-color: #d6b6cd;
}
.card.cardComedia {
	background-color: #97b2ea;
}
.card.cardAventura {
	background-color: #4c9fa2;
}

.resumo p{
	font-size: 0.8em;
}









 </style>


    <script type="text/javascript">
        /* --- select --*/
        $(document).ready(function () {
            $('select').formSelect();
        });

        /* --- navbar ---*/
        $(function () {
            $(window).on('scroll', function () {
                var nav = $('.scrolling-navbar');
                if ($(window).scrollTop() > 20) {
                    nav.css({
                        "padding-top": "45px",
                        "padding-bottom": "5px",
                        "position": "fixed",
                        "margin-top": "-65px"
                    });
                    nav.addClass('top-nav-collapse');
                }
                else {
                    nav.css({
                        "padding-top": "25px",
                        "padding-bottom": "12px",
                        "position": "relative",
                        "margin-top": "0"
                    });
                    nav.removeClass('top-nav-collapse');
                }
            });
        });


        /*--- filter ---*/
        // Filter & Sort Menus
        // -----------------------------


        // ONLOAD - Hide Filter Section & Sub-Menus (Precaution - Should already be set to display: none)
        $(document).ready(function () {
            var $filterGrid = $('.js-filter-grid'),
                $filterClearLinks = $('.js-clear-active-filter-links'),
                $filterItem = $('.js-filter-item'),
                requiredFilters = '',
                optionalFilters = '',
                filtersRegex = '';


            // BASE UI
            // ---------------------------------------------------------------------

            $('.filter-menu').hide();           // Hides Filter Section
            $('.js-filter-items-toggle').hide();// Hides Filter List
            $('.filter-caret').hide();          // Hides Filter Caret Image
            $('.sort-caret').hide();            // Hides Sort Caret Image

            // Initialize isotope on the grid

            $filterGrid.isotope({
                itemSelector: '.js-filter-grid-item',
                layoutMode: 'fitRows',
                getSortData: {
                    date: '[data-pub-date] parseInt',
                    dateRev: '[data-pub-date] parseInt',
                    featured: function (itemElem) {
                        return $(itemElem).data('is-featured') == '' ? 0 : 1;
                    },
                    rank: '[data-page-hits] parseInt',
                    topic: '[data-topic]'
                },
                sortAscending: {
                    date: false,
                    dateRev: true,
                    featured: false,
                    rank: false,
                    topic: true
                }
            });
            //=====================================================
            // Filter Area Here


            // Filter from one select box OR another but not from both at the same time
            $('.video-filter').on('change', function () {
                // get filter value from option value
                var filterValue = this.value;
                $filterGrid.isotope({ filter: filterValue });
                console.log(filterValue);
            });
            $('#category-filter').change(function () {
                $('#topic-filter option:first').prop('selected', 'selected');
            });


            $('#topic-filter').change(function () {
                $('#category-filter option:first').prop('selected', 'selected');
            });

            $(".clear-filter").click(function () {
                $('.video-filter').val('*').trigger('change');
            });

        });
    </script>



<div class="col col s12 m6 l4 cardContainer js-filter-item js-filter-grid-item movie romance" >
            <div class="card cardRomance">
					    <div class="card-image waves-effect waves-block waves-light">
					      <img class="activator" src="http://t0.gstatic.com/images?q=tbn:ANd9GcRW8IcoIkyofkw7XeIaty_05_jNg8XXf_tYNisCJqXOeMF9yhSG">
					    </div>
					    <div class="card-content">
					      <span class="card-title activator grey-text text-darken-4">Uma razão para recomeçar<i class="material-icons right">more_vert</i></span>
					    </div>
					    <div class="card-reveal">
					      	<span class="card-title grey-text text-darken-4">Informações:<i class="material-icons right">close</i></span>
                  <div class="tipo">
                    <p>Movie</p>
                  </div>
                  <div class="ano">
                    <p>Ano: 2016</p>
                  </div>                  
                  <div class="resumo"> 
                    <p>Sinópse:</p>
                    <p>
                      Amigos de infância embarcam na aventura de viverem juntos, mas uma tragédia pode estragar o futuro.
                    </p>
                  </div>	
					    </div>
					</div>
	      </div>	



</body>
</html>
