﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Search" MasterPageFile="~/MasterForHome.master" %>

<%@ Register Src="~/UserControl/FltSearch.ascx" TagName="IBESearch" TagPrefix="Search" %>
<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="Search" TagName="HotelSearch" %>
<%@ Register Src="~/BS/UserControl/BusSearch.ascx" TagName="BusSearch" TagPrefix="Searchsss" %>
<%@ Register Src="~/UserControl/FltSearchFixDep.ascx" TagName="IBESearchDep" TagPrefix="SearchDep" %>
<%@ Register Src="~/UserControl/Fare_Cal.ascx" TagName="FareCal" TagPrefix="FDCAL" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Cont1" runat="server">
    <link type="text/css" href="Advance_CSS/dropdown_search_box/css/select2.min.css" rel="stylesheet" />
    <link href="Advance_CSS/css/Search.css" rel="stylesheet" />
    <link type="text/css" href="Custom_Design/css/search.css" rel="stylesheet" />
    <link href="Advance_CSS/css/Fair-Calander.css" rel="stylesheet" />
    <link href="icofont/icofont.css" rel="stylesheet" />
    <link href="icofont/icofont.min.css" rel="stylesheet" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>


    <style type="text/css">
        .tab__list {
            display:none !important;
        }

        #resource-slider {
            position: relative;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            height: 13em;
            margin: auto;
            border-radius: 3px;
            background: #fff;
            /*border: 1px solid #DDD;*/
            overflow: hidden;
        }
.bootstrap-select.btn-group .dropdown-menu {
    min-width: 108% !important;
    z-index: 1035;
    margin-left: -40px  !important;
    margin-top: -43px  !important;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
            #resource-slider .arrow {
                cursor: pointer;
                position: absolute;
                width: 2em;
                height: 0;
                padding: 0;
                margin: 0;
                outline: 0;
                background: transparent;
            }

                #resource-slider .arrow:hover {
                    background: rgba(0, 0, 0, 0.1);
                }

                #resource-slider .arrow:before {
                    content: '';
                    position: absolute;
                    top: 0;
                    left: 0;
                    right: 0;
                    bottom: 0;
                    width: 0.75em;
                    height: 0.75em;
                    margin: auto;
                    border-style: solid;
                }

            #resource-slider .prev {
                left: 0;
                bottom: 0;
            }

                #resource-slider .prev:before {
                    left: 0.25em;
                    border-width: 3px 0 0 3px;
                    border-color: #333 transparent transparent #333;
                    transform: rotate(-45deg);
                }

            #resource-slider .next {
                right: 0;
                bottom: 0;
            }

                #resource-slider .next:before {
                    right: 0.25em;
                    border-width: 3px 3px 0 0;
                    border-color: #333 #333 transparent transparent;
                    transform: rotate(45deg);
                }

            #resource-slider .resource-slider-frame {
                position: absolute;
                top: 0;
                left: 2em;
                right: 2em;
                bottom: 0;
                border-left: 0.25em solid transparent;
                border-right: 0.25em solid transparent;
                overflow: hidden;
            }

            #resource-slider .resource-slider-item {
                position: absolute;
                top: 0;
                bottom: 0;
                width: 25%;
                height: 100%;
            }

            #resource-slider .resource-slider-inset {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                margin: 0.5em 0.25em;
                /*overflow: hidden;*/
            }

        @media ( max-width: 60em ) {
            #resource-slider .resource-slider-item {
                width: 33.33%;
            }

            #resource-slider {
                height: 16em;
            }
        }

        @media ( max-width: 45em ) {
            #resource-slider .resource-slider-item {
                width: 50%;
            }
        }

        @media ( max-width: 30em ) {
            #resource-slider .resource-slider-item {
                width: 100%;
            }

            #resource-slider {
                height: 19em;
            }
        }
    </style>

    <style type="text/css">
        .page-title {
            text-align: center;
            color: #FFFFFF;
            font-weight: 100;
            font-size: 40px;
            margin-top: 60px;
            margin-bottom: 40px;
        }

        .ticket-card {
            /*margin-top: 15vh;*/
            margin-bottom: 15vh;
            background: #FFFFFF;
            border-radius: 4px;
            box-shadow: 0 10px 45px rgb(0 0 0 / 42%), 0 2px 4px rgb(0 0 0 / 10%);
        }

            .ticket-card:hover .cover img, .ticket-card.active .cover img {
                -moz-transform: translate(0, -50px);
                -o-transform: translate(0, -50px);
                -ms-transform: translate(0, -50px);
                -webkit-transform: translate(0, -50px);
                transform: translate(0, -50px);
                box-shadow: 0 10px 20px -4px rgba(22, 22, 22, 0.5);
            }

            .ticket-card .cover {
                border-radius: 4px 4px 0 0;
                position: relative;
                margin: 15px;
            }

                .ticket-card .cover img {
                    width: 100%;
                    position: relative;
                    z-index: 2;
                    margin-top: -30px;
                    box-shadow: 0 10px 16px -6px rgba(22, 22, 22, 0.5);
                    border-radius: 4px;
                    -moz-transform: translate(0, 0);
                    -o-transform: translate(0, 0);
                    -ms-transform: translate(0, 0);
                    -webkit-transform: translate(0, 0);
                    transform: translate(0, 0);
                    -webkit-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 300ms ease, opacity 300ms ease;
                    -moz-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 300ms ease, opacity 300ms ease;
                    -o-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 300ms ease, opacity 300ms ease;
                    -ms-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 300ms ease, opacity 300ms ease;
                    transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 300ms ease, opacity 300ms ease;
                }

                .ticket-card .cover .info {
                    position: absolute;
                    width: 100%;
                    bottom: 0px;
                    padding: 0 15px;
                    color: #777777;
                }

                    .ticket-card .cover .info .going,
                    .ticket-card .cover .info .tickets-left {
                        padding-bottom: 10px;
                        border-bottom: 1px solid #f3f3f3;
                        width: 50%;
                    }

                    .ticket-card .cover .info .going {
                        float: left;
                    }

                    .ticket-card .cover .info .tickets-left {
                        float: right;
                        text-align: right;
                    }

                    .ticket-card .cover .info .fa {
                        color: #CCCCCC;
                        margin-right: 5px;
                    }

            .ticket-card .artist {
                float: left;
            }

                .ticket-card .artist .info {
                    font-weigth: 600;
                    font-size: 12px;
                    text-transform: uppercase;
                    color: #BBBBBB;
                    margin-bottom: 0;
                }

                .ticket-card .artist .name {
                    font-weight: 200;
                    font-size: 22px;
                    margin-top: 5px;
                }

            .ticket-card .ticket {
                float: left;
            }

                .ticket-card .ticket small {
                    font-size: 75%;
                }

            .ticket-card .price {
                float: right;
                text-align: right;
            }

                .ticket-card .price .from {
                    color: #BBBBBB;
                }

                .ticket-card .price .value {
                    font-size: 28px;
                    font-weight: 200;
                    color: #00bbff;
                    margin-top: -5px;
                }

                    .ticket-card .price .value b {
                        font-size: 18px;
                        font-weight: 200;
                    }

            .ticket-card .list-unstyled {
                max-height: 200px;
                overflow-x: hidden;
                overflow-y: scroll;
                background: #EEEEEE;
                margin-bottom: 0;
                
            }

                .ticket-card .list-unstyled li {
                    border-bottom: 1px dotted #CCCCCC;
                    padding: 5px 30px;
                    overflow: hidden;
                    width: 100%;
                    display: block;
                    position: relative;
                }

                    .ticket-card .list-unstyled li .btn-buy {
                        position: absolute;
                        right: 15px;
                        top: 13px;
                        padding: 8px 20px;
                        border-radius: 6px;
                        background: #00bbff;
                        border: 0;
                        opacity: 0;
                        -webkit-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 300ms ease, opacity 300ms ease;
                        -moz-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 300ms ease, opacity 300ms ease;
                        -o-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 300ms ease, opacity 300ms ease;
                        -ms-transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 300ms ease, opacity 300ms ease;
                        transition: transform 300ms cubic-bezier(0.34, 2, 0.6, 1), box-shadow 300ms ease, opacity 300ms ease;
                    }

                    .ticket-card .list-unstyled li:hover .btn-buy {
                        opacity: 1;
                    }

                    .ticket-card .list-unstyled li:last-child {
                        border-bottom: none;
                    }

                    .ticket-card .list-unstyled li:before, .ticket-card .list-unstyled li:after {
                        display: table;
                        content: " ";
                        clear: both;
                    }

                    .ticket-card .list-unstyled li .price .value {
                        color: #444444;
                        font-size: 22px;
                        margin-top: 10px;
                    }

            .ticket-card .body {
                padding: 5px 30px;
            }

                .ticket-card .body .info {
                    color: #777777;
                }

                .ticket-card .body .location,
                .ticket-card .body .date {
                    padding-top: 10px;
                    /*width: 50%;*/
                }

                .ticket-card .body .location {
                    float: left;
                }

                .ticket-card .body .date {
                    float: right;
                    text-align: right;
                }

                .ticket-card .body .fa {
                    color: #CCCCCC;
                    margin-right: 5px;
                }

            .ticket-card .footer .btn {
                width: 100%;
                background: transparent;
                border-top: 1px dotted #BBBBBB;
                border-radius: 0;
                padding: 15px 8px;
                font-size: 12px;
                font-weight: 600;
                text-transform: uppercase;
                color: #666666;
                box-shadow: none;
            }

                .ticket-card .footer .btn:focus, .ticket-card .footer .btn:hover, .ticket-card .footer .btn:active {
                    outline: none !important;
                }
    </style>


    <style type="text/css">
        #container {
            overflow-x: auto;
            width: 100%;
            overflow: hidden;
        }

        .T-fare, .tot-fair, .price-list {
            text-transform: uppercase;
        }

        body, html {
            font-family: 'Quicksand', sans-serif !important;
        }
    </style>

    <%--<script type="text/javascript">

        $(document).ready(function () {
            $('.minus').click(function () {
                var $input = $(this).parent().find('input');
                var $inputid = $input.attr('id');
                var count = parseInt($input.val()) - 1;
                if ($inputid != "Adult") {
                    count = count <= 0 ? 0 : count;
                }
                else {
                    count = count < 1 ? 1 : count;
                }
                $input.val(count);
                $input.change();
                AddAllPax();
                return false;
            });
            $('.plus').click(function () {
                var $input = $(this).parent().find('input');
                let inpcount = parseInt($input.val()) + 1;
                $input.val(inpcount);
                $input.change();
                AddAllPax();
                return false;
            });

            AddAllPax();

            function AddAllPax() {
                let adultinp = $("#Adult").val();
                let childinp = $("#Child").val();
                let infantinp = $("#Infant").val();

                $("#sapnTotPax").val(parseInt(adultinp) + parseInt(childinp) + parseInt(infantinp) + " Traveler(s)");


            }
        });
    </script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.minus1').click(function () {
                var $input = $(this).parent().find('input');
                var $inputid = $input.attr('id');
                var count = parseInt($input.val()) - 1;
                if ($inputid != "Adult") {
                    count = count <= 0 ? 0 : count;
                }
                else {
                    count = count < 1 ? 1 : count;
                }
                $input.val(count);
                $input.change();
                AddAllPax1();
                return false;
            });
            $('.plus1').click(function () {
                var $input = $(this).parent().find('input');
                let inpcount = parseInt($input.val()) + 1;
                $input.val(inpcount);
                $input.change();
                AddAllPax1();
                return false;
            });

            AddAllPax1();

            function AddAllPax1() {
                let adultinp = $("#AdultF1").val();
                let childinp = $("#ChildF1").val();
                let infantinp = $("#InfantF1").val();

                $("#sapnTotPaxF1").val(parseInt(adultinp) + parseInt(childinp) + parseInt(infantinp) + " Traveler(s)");


            }
        });
    </script>

    <style type="text/css">
        .mySlides img {
            vertical-align: middle;
            width: 100% !important;
        }
        /* Slideshow container */
        .slideshow-container {
            max-width: 1000px;
            position: relative;
            margin: auto;
        }

        /* Next & previous buttons */
        .prev, .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 5px;
            margin-top: -22px;
            color: white;
            font-weight: bold;
            font-size: 18px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover, .next:hover {
                background-color: rgba(0,0,0,0.8);
            }


        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }



        @-webkit-keyframes fade {
            from {
                opacity: .4;
            }

            to {
                opacity: 1;
            }
        }

        @keyframes fade {
            from {
                opacity: .4;
            }

            to {
                opacity: 1;
            }
        }

        /* On smaller screens, decrease text size */
        @media only screen and (max-width: 300px) {
            .prev, .next, .text {
                font-size: 11px;
            }
        }
    </style>

    <style type="text/css">
        .head {
            position: relative;
            background: linear-gradient(60deg, rgba(84,58,183,1) 0%, rgba(0,172,193,1) 100%);
            color: white;
        }

        .logo {
            width: 50px;
            fill: white;
            padding-right: 15px;
            display: inline-block;
            vertical-align: middle;
        }

        .inner-head {
            height: 65vh;
            width: 100%;
            margin: 0;
            padding: 0;
        }

        .flex { /*Flexbox for containers*/
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
        }

        .waves {
            position: relative;
            width: 100%;
            height: 15vh;
            margin-bottom: -7px; /*Fix for safari gap*/
            min-height: 100px;
            max-height: 150px;
        }

        .content {
            position: relative;
            height: 20vh;
            text-align: center;
            background-color: white;
        }

        /* Animation */

        .parallax > use {
            animation: move-forever 25s cubic-bezier(.55,.5,.45,.5) infinite;
        }

            .parallax > use:nth-child(1) {
                animation-delay: -2s;
                animation-duration: 7s;
            }

            .parallax > use:nth-child(2) {
                animation-delay: -3s;
                animation-duration: 10s;
            }

            .parallax > use:nth-child(3) {
                animation-delay: -4s;
                animation-duration: 13s;
            }

            .parallax > use:nth-child(4) {
                animation-delay: -5s;
                animation-duration: 20s;
            }

        @keyframes move-forever {
            0% {
                transform: translate3d(-90px,0,0);
            }

            100% {
                transform: translate3d(85px,0,0);
            }
        }
        /*Shrinking for mobile*/
        @media (max-width: 768px) {
            .waves {
                height: 40px;
                min-height: 40px;
            }

            .content {
                height: 30vh;
            }

            h1 {
                font-size: 24px;
            }
        }
    </style>




    <%--<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #fff!important">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">Close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="slideshow-container">
                        <%=NotificationContent %>
                        <%--<a class="prev" onclick="plusSlides(-1)" style="background: rgb(238 49 87);">&#10094;</a>
                        <a class="next" onclick="plusSlides(1)" style="background: rgb(238 49 87);">&#10095;</a>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

    <%--<script type="text/javascript">	
			function isEmpty(el) { return !$.trim(el.html()) }
			if (isEmpty($('.mySlides'))) {
				
			}
			else {
				 $(window).load(function () {$('#exampleModal').modal('show'); });
			}
		
			//$(window).load(function () {
				//$('#exampleModal').modal('show'); 			
        //});
			$('.carousel').carousel();
	
            var slideIndex = 1;
            showSlides(slideIndex);

            function plusSlides(n) {
                showSlides(slideIndex += n);
            }
            function showSlides(n) {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("dot");
                if (n > slides.length) { slideIndex = 1 }
                if (n < 1) { slideIndex = slides.length }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
            }
        </script>--%>

    <%--<a class="btn btn-default" id="btn-confirm" style="position: absolute; top: 73px; z-index: 999;">Confirm</a>--%>
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel1" aria-hidden="true" id="mi-modal" style="z-index: 99990 !important;">
        <div class="modal-dialog modal-sm" style="margin-top: 10%; width: 30%;">
            <div class="modal-content">
                <%--  <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmar</h4>
      </div>--%>
                <div class="modal-body">
                    <h5>Are you sure you want to continue with this flight?</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="modal-btn-cancel" style="float: left; background: #ff0000">Cancel</button>
                    <button type="button" class="btn btn-success" id="modal-btn-confirm">Confirm</button>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel2" aria-hidden="true" id="flt-details" style="z-index: 9999 !important;">
        <div class="modal-dialog modal-sm" style="margin-top: 10%; width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button>
                    <%-- <h4 class="modal-title" id="myModalLabel">Confirmar</h4>--%>
                </div>
                <div class="modal-body">
                    <div class="tabs">
                        <div value="1api6ENRMLITZNRML" class="fare-groups-wrap 1api6ENRMLITZNRML_faredetailmasterall " id="1api6ENRMLITZNRML_faredetailmaster" style="margin-bottom: 40px;">
                            <div class="gridViewToolTip1 lft" title="1api6ENRMLITZNRML_O"></div>
                            <ul class="fare-groups nav navbar-nav" role="tablist" style="border-bottom: 1px solid #CCC;">
                                <li class="fgf sel nav-item active" id="1api6ENRMLITZNRML_Allll"><a href="#1api6ENRMLITZNRML_Fare" data-toggle="tab" role="tab" class="div_cls d collapsible gridViewToolTip nav-link active" style="padding: 0px;" id="fare-summ">Fare Details</a>
                                </li>
                                <li class="fgf nav-item"><a href="#1api6ENRMLITZNRML_fltdt" data-toggle="tab" role="tab" class="div_cls d collapsible fltDetailslink nav-link" id="fare_Det" style="padding: 0px;">Flight Details</a>
                                </li>
                                <li class="fgf nav-item"><a href="#1api6ENRMLITZNRML_bag" data-toggle="tab" role="tab" class="div_cls d collapsible fltBagDetails nav-link" id="bag_det" style="padding: 0px;">Baggage</a>
                                </li>
                                <li class="fgf nav-item"><a href="#1api6ENRMLITZNRML_canc" data-toggle="tab" role="tab" class="div_cls d collapsible fareRuleToolTip cursorpointer nav-link" style="padding: 0px;" id="can_flt">Cancellation</a><div class="fade" title="1api6ENRMLITZNRML_O"></div>

                                </li>
                                <div class="ui_block clearfix"></div>
                            </ul>
                        </div>
                        <div class="tabs-stage tab-content">
                            <div class="depcity tab-pane active" role="tabpanel" id="1api6ENRMLITZNRML_Fare" style="margin-top: -11px;">
                                <div><span onclick="Close('1api6ENRMLITZNRML_');" title="Click to close Details"></span></div>
                                <div class="new-fare-details " id="FareBreak">
                                    <img src="Images/loading_bar.gif" />
                                </div>
                                <div class="clear"></div>
                                <div class="clear"></div>
                            </div>
                            <div class="depcity tab-pane" role="tabpanel" id="1api6ENRMLITZNRML_fltdt" style="margin-top: -11px;"></div>
                            <div class="depcity tab-pane" role="tabpanel" id="1api6ENRMLITZNRML_bag" style="margin-top: -11px;"></div>
                            <div class="depcity tab-pane" role="tabpanel" id="1api6ENRMLITZNRML_canc" style="margin-top: -11px;"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel3" aria-hidden="true" id="pax-mod">
        <div class="modal-dialog modal-sm" style="margin-top: 10%; width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button>
                </div>
                <div class="modal-body">



                    <%--<h5>Please select travelers first to make booking.</h5>--%>

                    <div class="row" data-gutter="none">
                        <div id="fltpx">
                        </div>
                        <div class="col-md-12" id="collapseExample4">


                            <input class="aumd-tb div1" id="sapnTotPaxF1" placeholder=" Traveller" type="text" disabled="disabled" />

                            <div id="div_Adult_Child_Infant1hj" class="myText">
                                <div class="innr_pnl dflex">
                                    <div class="main_dv pax-limit">
                                        <label>
                                            <span>Adult</span>

                                        </label>
                                        <div class="number">
                                            <span class="minus1">-</span>
                                            <input type="text" class="inp" value="1" min="1" name="Adult" id="AdultF1">
                                            <span class="plus1">+</span>
                                        </div>

                                    </div>
                                    <div class="main_dv pax-limit" style="margin-left: 70px;">
                                        <label>
                                            <span>Child<span class="light-grey">(2+ 12 yrs)</span></span>

                                        </label>

                                        <div class="number">
                                            <span class="minus1">-</span>
                                            <input type="text" class="inp" value="0" min="0" name="Child" id="ChildF1">
                                            <span class="plus1">+</span>
                                        </div>

                                    </div>
                                    <div class="main_dv pax-limit" style="margin-left: 70px;">

                                        <label>
                                            <span>Infant <span class="light-grey">(below 2 yrs)</span></span>

                                        </label>

                                        <div class="number">
                                            <span class="minus1">-</span>
                                            <input type="text" class="inp" value="0" min="0" name="Infant" id="InfantF1">
                                            <span class="plus1 Infant1">+</span>
                                        </div>

                                    </div>

                                </div>
                            </div>


                        </div>



                    </div>

                </div>
                <div class="modal-footer hidden">
                    <%--<button type="button" class="btn btn-danger" id="pax-confirm" style="float: left; background: #ff0000">Cancel</button>--%>
                    <button type="button" class="btn btn-success" id="pax-confirm">Confirm</button>

                </div>
            </div>
        </div>
    </div>

    <input type="hidden" value="" id="pass-pax" />




    <div class="theme-hero-area theme-hero-area-primary">

        <div class="theme-hero-area-body">
            <div class="_pt-250 _pb-200 _pv-mob-50">
                <section> <!--flight-wrapper-->
                    <div class="theme-search-area-tabs vertical_search_engine">


                        <div class="tab-content">
                            <div class="head">


<div class="inner-header">



                            <div class="container">
                               <%-- <h4 style="padding: 15px;">Book Your Flight</h4>--%>
                           <div class="theme-search-area-tabs-header _c-w _ta-mob-c">
                <h1 class="theme-search-area-tabs-title">Book Your Fixed Departure Flights</h1>
                <p class="theme-search-area-tabs-subtitle">Compare hundreds travel websites at once</p>
              </div>

                            <div class="tab-pane fade in active" id="tab1default1">

                                <Search:IBESearch ID="IBESearch2" runat="server" />
                                <SearchDep:IBESearchDep runat="server" ID="FixDep" />

 </div>


                            </div>
                          
                            <div class="tab-pane fade" id="tab2default" style="display:none;">
                                <Searchsss:BusSearch ID="Bus2" runat="server" />
                            </div>


                        </div>
<div>
<svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
<defs>
<path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
</defs>
<g class="parallax">
<use xlink:href="#gentle-wave" x="48" y="0" fill="rgba(255,255,255,0.7" />
<use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.5)" />
<use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.3)" />
<use xlink:href="#gentle-wave" x="48" y="7" fill="#fff" />
</g>
</svg>
</div>

     
                                </div>


</div>

                    </div>

                    <div class="container">
                        <div class="offer-banner trip-section fluid-full-width moduleArea hidden">
                            <div class="Trip-book why_RiyaTrip moduleArea">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="/design/Home.aspx" role="button">Home</a>
                                    </li>
                                    <li>
                                        <a href="/design/Home.aspx" role="button">Flight</a>

                                    </li>
                                    <li>
                                        <a href="About-Us.aspx" role="button">About</a>

                                    </li>
                                    <li>
                                        <a href="Contact.aspx" role="button">Contact</a>

                                    </li>

                                    <li class="navbar-nav-item-user dropdown hidden">
                                        <a class="dropdown-toggle" href="account.html" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-user-circle-o navbar-nav-item-user-icon"></i>My Account
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="Account.aspx">Preferences</a>
                                            </li>
                                            <li>
                                                <a href="#">Notifications</a>
                                            </li>
                                            <li>
                                                <a href="#">Payment Methods</a>
                                            </li>
                                            <li>
                                                <a href="#">Travelers</a>
                                            </li>
                                            <li>
                                                <a href="#">History</a>
                                            </li>

                                        </ul>
                                    </li>

                                </ul>
                            </div>
                        </div>

                        <FDCAL:FareCal ID="FDCAL1" runat="server" />

                        <div class="offer-banner trip-section fluid-full-width moduleArea" id="div_LatestOffers">

                            <div class="Trip-book why_RiyaTrip moduleArea">
<%--                                <span class="module_heading" style="font-size: 15px; color: #000; float: left; margin-top: -15px; margin-bottom: 5px;">Latest Offer!</span>--%>
                                <div class="theme-page-section-header _ta-l">
          <h5 class="theme-page-section-title">Our New FDD Flights Available</h5>
          <p class="theme-page-section-subtitle"><a>Filter Sectors</a></p>
          <a class="theme-page-section-header-link theme-page-section-header-link-rb" href="#">Filter Option</a>
                                    <div class="col-md-4"> 
                                        <asp:DropDownList ID="ddlFromSector" runat="server" class="selectpicker" data-show-subtext="true" data-live-search="true">
                                            <asp:ListItem>Select From Sector</asp:ListItem>
                                           
                                        </asp:DropDownList>
             
                                        </div>

                                     <div class="col-md-4"> 
                                        <asp:DropDownList ID="ddLToSector" runat="server" class="selectpicker" data-show-subtext="true" data-live-search="true">
                                            <asp:ListItem>Select From Sector</asp:ListItem>
                                           
                                        </asp:DropDownList>
             
                                        </div>
        </div>
                                <div id="avlsector_id" style="display: none;">
                                  <%--  <b class='status_info fl'>
                                        <i class='icofont-location-pin icofont-2x'></i></b>
                                    <span class='status_cont'>Avilable Sector</span>
                                     <button id="slideFront" type="button" style="float:right;border:none;background:none;"><i class="icofont-dotted-right icofont-2x"></i></button>
                                    <button id="slideBack" type="button" style="float:right;border:none;background:none;"><i class="icofont-dotted-left icofont-2x"></i></button>--%>
                                   
                                </div>

                                <div class="tab">
                                    <div id="container">
                                        <style>
                                            div#field-dropdown-autocomplete {
                                                width: 30%;
                                                display: none;
                                            }
                                        </style>
                                        <select name="field-dropdown"  id="field-dropdown" style="display:none;">
                                            <option value="">Select</option>
                                        </select>

                                    </div>
                                    <div id="BindDepArrDetails">
                                        <p class="text-center" style="font-size: 20px;">Please wait... <i class="fa fa-spinner fa-pulse"></i></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                            var button = document.getElementById('slideFront');
                            button.onclick = function () {
                                var container = document.getElementById('container');
                                sideScroll(container, 'right', 25, 100, 10);
                            };

                            var back = document.getElementById('slideBack');
                            back.onclick = function () {
                                var container = document.getElementById('container');
                                sideScroll(container, 'left', 25, 100, 10);
                            };

                            function sideScroll(element, direction, speed, distance, step) {
                                scrollAmount = 0;
                                var slideTimer = setInterval(function () {
                                    if (direction == 'left') {
                                        element.scrollLeft -= step;
                                    } else {
                                        element.scrollLeft += step;
                                    }
                                    scrollAmount += step;
                                    if (scrollAmount >= distance) {
                                        window.clearInterval(slideTimer);
                                    }
                                }, speed);
                            }

                        </script>


                        <section class="offer-banner trip-section fluid-full-width moduleArea hidden">
                         <div class="anniversaryBannerText">
                         <a target="_blank" rel="noopener" title="Paisa Bazaar" href="#">
                             <img src="Advance_CSS/Cover.jpg" class="conta iner"></a>

                         </div></section>

                        

                        <div class="trending-trip trending-trip-box carousalModuleArea" id="expired-offer">
                           
                                              <div class="theme-page-section-header _ta-l">
          <h5 class="theme-page-section-title">Limited Offer</h5>
          <p class="theme-page-section-subtitle">Book your flights before it expire.</p>
<%--          <a class="theme-page-section-header-link theme-page-section-header-link-rb" href="#">+ Find More</a>--%>
        </div>
                            <div class="row resources ">
            <div class="container" id="resource-slider">
                             <span class="arrow prev"></span>
                <span class="arrow next"></span>
                              <div class="resource-slider-frame">
                                <div class="slide divLimitedOfferExpireSoon" id="slide">
                                    <p class="text-center" style="font-size: 15px; margin: 75px 10px 10px 300px;">Please wait... <i class="fa fa-spinner fa-pulse"></i></p>
                                </div>
                            </div>
                          </div>
                                </div>
                        </div>

                        


                        


                        
                    </div>
                </section>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        function defer(method) {
            if (window.jQuery)
                method();
            else
                setTimeout(function () {
                    defer(method)
                }, 50);
        }
        defer(function () {
            (function ($) {

                function doneResizing() {
                    var totalScroll = $('.resource-slider-frame').scrollLeft();
                    var itemWidth = $('.resource-slider-item').width();
                    var difference = totalScroll % itemWidth;
                    if (difference !== 0) {
                        $('.resource-slider-frame').animate({
                            scrollLeft: '-=' + difference
                        }, 500, function () {
                            // check arrows
                            checkArrows();
                        });
                    }
                }

                function checkArrows() {
                    var totalWidth = $('#resource-slider .resource-slider-item').length * $('.resource-slider-item').width();
                    var frameWidth = $('.resource-slider-frame').width();
                    var itemWidth = $('.resource-slider-item').width();
                    var totalScroll = $('.resource-slider-frame').scrollLeft();

                    if (((totalWidth - frameWidth) - totalScroll) < itemWidth) {
                        $(".next").css("visibility", "hidden");
                    }
                    else {
                        $(".next").css("visibility", "visible");
                    }
                    if (totalScroll < itemWidth) {
                        $(".prev").css("visibility", "hidden");
                    }
                    else {
                        $(".prev").css("visibility", "visible");
                    }
                }

                $('.arrow').on('click', function () {
                    var $this = $(this),
                        width = $('.resource-slider-item').width(),
                        speed = 500;
                    if ($this.hasClass('prev')) {
                        $('.resource-slider-frame').animate({
                            scrollLeft: '-=' + width
                        }, speed, function () {
                            // check arrows
                            checkArrows();
                        });
                    } else if ($this.hasClass('next')) {
                        $('.resource-slider-frame').animate({
                            scrollLeft: '+=' + width
                        }, speed, function () {
                            // check arrows
                            checkArrows();
                        });
                    }
                }); // end on arrow click

                $(window).on("load resize", function () {
                    checkArrows();
                    $('#resource-slider .resource-slider-item').each(function (i) {
                        var $this = $(this),
                            left = $this.width() * i;
                        $this.css({
                            left: left
                        })
                    }); // end each
                }); // end window resize/load

                var resizeId;
                $(window).resize(function () {
                    clearTimeout(resizeId);
                    resizeId = setTimeout(doneResizing, 500);
                });

            })(jQuery); // end function
        });
    </script>





    <script type="text/javascript">
        $('.toggle-tickets').click(function () {
            $tickets = $(this).parent().siblings('.collapse');

            if ($tickets.hasClass('in')) {
                $tickets.collapse('hide');
                $(this).html('Show Tickets');
                $(this).closest('.ticket-card').removeClass('active');
            } else {
                $tickets.collapse('show');
                $(this).html('Hide Tickets');
                $(this).closest('.ticket-card').addClass('active');
            }
        });
    </script>





    <script type="text/javascript" src="Advance_CSS/dropdown_search_box/js/select2.min.js"></script>






    <script type="text/javascript">
        $(window).load(function () {
            if (localStorage.getItem('isnotification') != 'notyshown') {
                $('#exampleModal').modal('hide');
                localStorage.setItem('isnotification', 'notyshown');
            }
        });
    </script>
</asp:Content>
