﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="contact-us.aspx.vb" Inherits="contact_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section>
        <div class="rows contact-map map-container">
            <div style="width: 100%; position: relative; margin-top: 70px;">

                <div style="width: 100%; position: relative;">
                    <iframe width="100%" height="300" src="https://maps.google.com/maps?width=700&amp;height=440&amp;hl=en&amp;q=Riya%20Trip%20and%20Cabs%2C%20Gandhi%20Maidan%2C%20Patna%2C%20Bihar+(Riya%20Trip)&amp;ie=UTF8&amp;t=&amp;z=10&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    <div style="position: absolute; width: 80%; bottom: 10px; left: 0; right: 0; margin-left: auto; margin-right: auto; color: #000; text-align: center;"><small style="line-height: 1.8; font-size: 2px; background: #fff;">Powered by <a href="http://www.googlemapsgenerator.com/ja/">Googlemapsgenerator.com/ja/</a> & <a href="https://www.cusl.se/">cusl.se</a></small></div>
                    <style>
                        #gmap_canvas img {
                            max-width: none !important;
                            background: none !important
                        }
                    </style>
                </div>
                <br />



            </div>
    </section>

    <section>
        <div class="form form-spac rows con-page">
            <div class="container">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title col-md-12">
                    <h2><span>Contact us</span></h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <%--					<p>World's leading tour and travels Booking website. Book flight tickets and enjoy your holidays with distinctive experience</p>--%>
                </div>

                <div class="pg-contact">

                    <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con1">
                        <img src="img/contact/1.html" alt="">
                        <h4>Address</h4>
                        <p>
                            Gandhi Maidan, Patna, Bihar
                        </p>

                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con3">
                        <img src="img/contact/2.html" alt="">
                        <h4>24/7 Customer Support
                        </h4>
                        <p>Phone : +91-9955576731 / 9334888588</p>
                        <p>Email : help@riyatrip.com</p>
                    </div>
                    <%--<div class="col-md-3 col-sm-6 col-xs-12 new-con new-con4">
                        <img src="img/contact/3.html" alt="">
                        <h4>Sales Support</h4>
                        <p>Phone :  (011) 43554899</p>
                        <p>
                            Email : sales@flyagainonline.net
                            
                        </p>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con4">
                        <img src="img/contact/3.html" alt="">
                        <h4>Accounts & Balance Uploads </h4>
                        <p>Mhone : (011) 43554899 </p>
                        <p>Email : admin@flyagainonline.net</p>
                    </div>--%>
                </div>
            </div>
        </div>
    </section>




</asp:Content>

